O candidato deverá fazer a coleta de informações do site https://stackoverflow.com/ da seguinte forma:

##  **Parte I – Coleta dos dados (obrigatório)**

1. Fazer uma busca na página inicial (ex: python);

2. Coletar as informações de cada *post* da primeira e segunda página da busca realizada;

3. As informações de cada *post* podem ser dividas em: pergunta, respostas, comentários da pergunta e comentários da(s) resposta(s). Para cada uma destas informações, para cada *post*, pede-se para coletar:
	- Perguntas - Título, texto, autor, data, e tags;
	- Respostas - Título, texto, autor e data;
	- Comentários - Texto, autor e data.

  Caso na pergunta/resposta conste uma data de edição, deve ser coletada apenas a data na qual a pergunta/resposta foi feita. Para fins práticos, o autor da pergunta/resposta é sempre quem a fez, e não quem fez uma edição (caso tenha alguma edição).
 
  Caso os comentários de uma pergunta/resposta tenham que ser expandidos (*show # more comments*), estes também devem ser coletados;

4. Os dados coletados devem ser organizados em arquivo(s) no formato que o candito preferir (csv, xml, json, etc.). 

Obs.: i) Nos casos dos itens 1. e 2., tanto a busca quanto a "paginação" devem ser feitos por meio de programação e sem utilizar *hard coding* (inserir os links manualmente no arquivo do programa). ii) Além disso, apesar da existência de uma API pública do Stack Overflow, o objetivo deste exercício não é utilizá-la.

##  **Parte II – Disponibilização dos dados por meio de uma API**



5. Utilizando o framework de sua preferência (em Python), desenvolva uma API que contenha apenas um *endpoint GET*. Este *endpoint* deve receber um nome de autor:
```
{
   "autor" : "João"
}
```
E retornorar os textos (lista) refentes à este autor, consumindo-os a partir do arquivo que salvou no item 4 acima:
```
{
   "perguntas" : [],
   "respostas" : [],
   "comentarios" : []
}
```

Obs.: i) o não cumprimento da parte II da tarefa não desqualifica o candidato. Pense na parte II da tarefa como um diferencial em caso de empate. ii) Não é necessário hospedar a API em nenhum serviço, basta construir o código. A equipe de avaliação rodará localmente tal código.

##  **Entregáveis**

- Parte I (obrigatório): código fonte documentado e arquivo de dados documentado;
- Parte II: API feita em Flask documentada, código de consulta à API documentado;
- O material acima deverá ser enviado para a Kognita como um link em um repositório git documentado da escolha do candidato.


## **Criatividade é bem-vinda:** 

Caso deseje incrementar a solução acima, fique à vontade. O proposto aqui é um ponto de partida mínimo.

## **Prazo de entrega:** 1 semana após o envio do teste


